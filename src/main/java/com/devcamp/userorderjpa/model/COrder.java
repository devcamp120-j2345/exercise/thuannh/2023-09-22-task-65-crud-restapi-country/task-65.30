package com.devcamp.userorderjpa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

import javax.persistence.Column;

@Entity
@Table (name = "pizza_order")
public class COrder {
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private long id;
    
    @Column(name = "order_code", unique = true)
    private String orderCode;
    
    @Column(name = "pizza_size")
    private String pizzaSize;
    
    @Column(name = "pizza_type")
    private String pizzaType;
    
    @Column(name = "voucher_code")
    private String voucherCode;
    
    @Column(name = "price")
    private long price;
    
    @Column(name = "paid")
    private long paid;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at", nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_at", nullable = true)
    @LastModifiedDate
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date updated;

    @ManyToOne
    @JsonIgnore
    //@JoinColumn(name = "user_id")
    private CUser user;
    public COrder() {
        //super();
    }
    
    public COrder(long id, String orderCode, String pizzaSize, String pizzaType, String voucherCode, long price,
            long paid) {
        this.id = id;
        this.orderCode = orderCode;
        this.pizzaSize = pizzaSize;
        this.pizzaType = pizzaType;
        this.voucherCode = voucherCode;
        this.price = price;
        this.paid = paid;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getOrderCode() {
        return orderCode;
    }
    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }
    public String getPizzaSize() {
        return pizzaSize;
    }
    public void setPizzaSize(String pizzaSize) {
        this.pizzaSize = pizzaSize;
    }
    public String getPizzaType() {
        return pizzaType;
    }
    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }
    public String getVoucherCode() {
        return voucherCode;
    }
    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }
    public long getPrice() {
        return price;
    }
    public void setPrice(long price) {
        this.price = price;
    }
    public long getPaid() {
        return paid;
    }
    public void setPaid(long paid) {
        this.paid = paid;
    }
    public CUser getUser() {
        return user;
    }
    public void setUser(CUser user) {
        this.user = user;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    
}
