package com.devcamp.userorderjpa.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.userorderjpa.model.COrder;
import com.devcamp.userorderjpa.repository.IOrderRepository;

@Service
public class OrderService {
    @Autowired
    IOrderRepository orderRepository;
    public List<COrder> getAllOrders(){
        List<COrder> orders = new ArrayList<COrder>();
        orderRepository.findAll().forEach(orders::add);
        return orders;
    }
}
