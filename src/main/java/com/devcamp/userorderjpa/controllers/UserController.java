package com.devcamp.userorderjpa.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.userorderjpa.model.CUser;
import com.devcamp.userorderjpa.repository.IUserRepository;
import com.devcamp.userorderjpa.service.UserService;

@RestController
@RequestMapping("/v1/user")
@CrossOrigin
public class UserController {
    @Autowired
    private UserService userService;
   
    //get all user list
    @GetMapping("/all")
    public ResponseEntity<List<CUser>> getAllUsers(){
        try {
            return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    
    //get user detail by id
    @Autowired
    IUserRepository userRepository;
    @GetMapping("/detail")
    public ResponseEntity<Object> getUserById(@RequestParam(name="id", required = true)Long id){
        Optional<CUser> user = userRepository.findById(id);
        if (user.isPresent()){
            return new ResponseEntity<>(user, HttpStatus.OK);
        } else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    //create new user
    @PostMapping("/create")
    public ResponseEntity<Object> createUser(@RequestBody CUser pUser){
        try {
            pUser.setCreated(new Date());
            pUser.setUpdated(null);
            CUser _user = userRepository.save(pUser);
            return new ResponseEntity<>(_user, HttpStatus.CREATED);

        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //update user
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable(name="id")long id, @RequestBody CUser pUserUpdate){
        try {
           CUser user = userRepository.findById(id);
            if (user != null){
                user.setFullname(pUserUpdate.getFullname());
                user.setEmail(pUserUpdate.getEmail());
                user.setPhone(pUserUpdate.getPhone());
                user.setAddress(pUserUpdate.getAddress());
                user.setUpdated(new Date());
                
                return new ResponseEntity<>(userRepository.save(user), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //delete user by Id
    @DeleteMapping("/delete/{id}")
	public ResponseEntity<CUser> deleteUserById(@PathVariable("id") long id) {
		try {
			userRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
